package cmuguide.project.controller

import cmuguide.project.entity.Review
import cmuguide.project.entity.User
import cmuguide.project.entity.dto.ReviewDto
import cmuguide.project.service.ReviewService
import cmuguide.project.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
class ReviewController {
    @Autowired
    lateinit var reviewService: ReviewService
    @GetMapping("/reviews")
    fun getAllReviews(): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapReviewDto(reviewService.getAllReviews())
        return ResponseEntity.ok(output)
    }

    @GetMapping("/review/{id}")
    fun getReviewById(@PathVariable id:Long): ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReviewDto(reviewService.findById(id)))
    }

//    @PostMapping("/addreview/{shopId}")
//    fun addReview(@RequestBody review: ReviewDto,
//                  @PathVariable shopId:String): ResponseEntity<Any>{
//        val output = reviewService.save(shopId,MapperUtil.INSTANCE.mapReviewDto(review))
//        val outputDto = MapperUtil.INSTANCE.mapReviewDto(output)
//        outputDto?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }

    @PostMapping("/addreview/{shopId}/{email}")
    fun addReview(@RequestBody review: ReviewDto,
                  @PathVariable shopId:String,
                  @PathVariable email:String): ResponseEntity<Any>{
        val output = reviewService.save(shopId,email,MapperUtil.INSTANCE.mapReviewDto(review))
        val outputDto = MapperUtil.INSTANCE.mapReviewDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }


    @PutMapping("/editreview/{reviewId}")
    fun editReview(@RequestBody review: ReviewDto,
                   @PathVariable ("reviewId") id:Long): ResponseEntity<Any>{
        review.id = id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReviewDto(reviewService.editReview(id,review)))

    }
    @DeleteMapping("/deletereview/{reviewId}")
    fun deleteReview(@PathVariable ("reviewId") id:Long): ResponseEntity<Any>{
        val output = reviewService.remove(id)
        val outputDto = MapperUtil.INSTANCE.mapReviewDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
}