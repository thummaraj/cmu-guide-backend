package cmuguide.project.controller

import cmuguide.project.service.ShopService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@CrossOrigin("*")
@RestController
class ShopController {
    @Autowired
    lateinit var shopService: ShopService
    @GetMapping("/shop")
    fun getShop(): ResponseEntity<Any>{
        return ResponseEntity.ok(shopService.getShop())
    }
    @GetMapping("/shop/{shopId}")
    fun getShopByShopId(@PathVariable shopId:String): ResponseEntity<Any>{
        return ResponseEntity.ok(shopService.getShopByShopId(shopId))
    }

}