package cmuguide.project.repository

import cmuguide.project.entity.Shop
import org.springframework.data.repository.CrudRepository

interface ShopRepository:CrudRepository<Shop,Long> {
    fun findByShopIdContaining(shopId:String): Shop
}