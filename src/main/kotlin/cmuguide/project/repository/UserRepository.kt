package cmuguide.project.repository

import cmuguide.project.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository:CrudRepository<User,Long> {
    fun findByEmailContaining(email:String): User
}