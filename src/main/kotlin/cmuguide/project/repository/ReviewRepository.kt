package cmuguide.project.repository

import cmuguide.project.entity.Review
import org.springframework.data.repository.CrudRepository

interface ReviewRepository: CrudRepository<Review,Long>{
    fun findByIsDeletedIsFalse(): List<Review>

}