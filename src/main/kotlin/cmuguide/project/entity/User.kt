package cmuguide.project.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


@Entity
data class User (var email: String?= null,
                 var imageUrl: String? = null){
    @Id
    @GeneratedValue
    var id:Long? = null
}