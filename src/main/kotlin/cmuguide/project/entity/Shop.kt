package cmuguide.project.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class Shop (var shopId: String?= null,
                 var shopName: String?= null){
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany
    var reviews = mutableListOf<Review>()
}