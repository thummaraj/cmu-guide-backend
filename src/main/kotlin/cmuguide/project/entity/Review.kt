package cmuguide.project.entity

import java.time.LocalDate
import javax.persistence.*

@Entity
data class Review (var review: String?= null,
                   var rating: Int?= null,
                   var date: LocalDate? = null,
                   var isDeleted:Boolean = false){
    @Id
    @GeneratedValue
    var id:Long? = null

    @ManyToOne
     var user:User?= null
    constructor( review: String,
                 rating: Int,
                 date: LocalDate?,
                 user: User): this(review, rating, date){
        this.user = user
    }
}