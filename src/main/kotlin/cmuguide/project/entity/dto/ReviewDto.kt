package cmuguide.project.entity.dto

import cmuguide.project.entity.Shop
import cmuguide.project.entity.User
import java.time.LocalDate

data class ReviewDto (var review: String?= null,
                      var rating: Int?= null,
                      var date: LocalDate? = LocalDate.now(),
                      var user: User?= null,
                      var id:Long? = null)