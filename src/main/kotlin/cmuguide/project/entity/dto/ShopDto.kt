package cmuguide.project.entity.dto

data class ShopDto (var shopId: String?= null,
                    var shopName: String?= null,
                    var reviews: List<ReviewDto>? = emptyList())