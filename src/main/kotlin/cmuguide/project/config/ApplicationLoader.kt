package cmuguide.project.config

import cmuguide.project.entity.Review
import cmuguide.project.entity.Shop
import cmuguide.project.entity.User
import cmuguide.project.repository.ReviewRepository
import cmuguide.project.repository.ShopRepository
import cmuguide.project.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import java.time.LocalDate
import javax.transaction.Transactional

@Component
class ApplicationLoader:ApplicationRunner{

    @Autowired
    lateinit var reviewRepository: ReviewRepository
    @Autowired
    lateinit var shopRepository: ShopRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    override fun run(args: ApplicationArguments?) {
        val shop1 = shopRepository.save(Shop("5ca8e1dab488447ebb07b7c5","สตาร์"))
        val shop2 = shopRepository.save(Shop("5ca8e2ebb488447ebb07b7c6","ยิปโซ"))

        val review1 = reviewRepository.save(Review("Amazing",5, LocalDate.of(2019,4,20)))
        val review2 = reviewRepository.save(Review("love love love",5, LocalDate.of(2019,4,21)))

        val user1 = userRepository.save(User("jaja@gmail.com",null))


        review1.user = user1
        review2.user = user1

        shop1.reviews.add(review1)
        shop2.reviews.add(review2)


    }

}