package cmuguide.project.util

import cmuguide.project.entity.Review
import cmuguide.project.entity.dto.ReviewDto
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers


@Mapper(componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapReviewDto(review: Review): ReviewDto
    fun mapReviewDto(reviews: List<Review>): List<ReviewDto>
    @InheritInverseConfiguration
    fun mapReviewDto(review: ReviewDto): Review

}