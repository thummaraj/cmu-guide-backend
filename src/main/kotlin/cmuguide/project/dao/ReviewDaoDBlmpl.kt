package cmuguide.project.dao

import cmuguide.project.entity.Review
import cmuguide.project.repository.ReviewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ReviewDaoDBlmpl: ReviewDao {
    override fun findById(id: Long): Review {
        val output = reviewRepository.findById(id).orElse(null)
        return output
    }

    override fun save(review: Review): Review {
        return reviewRepository.save(review)
    }

    @Autowired
    lateinit var reviewRepository: ReviewRepository
    override fun getAllReviews(): List<Review> {
//        return reviewRepository.findAll().filterIsInstance(Review::class.java)
        return reviewRepository.findByIsDeletedIsFalse()
    }
}