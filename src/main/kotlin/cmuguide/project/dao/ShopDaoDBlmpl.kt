package cmuguide.project.dao

import cmuguide.project.entity.Shop
import cmuguide.project.repository.ShopRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ShopDaoDBlmpl:ShopDao {
    override fun getShopByShopId(shopId:String): Shop {
        return shopRepository.findByShopIdContaining(shopId)
    }

    @Autowired
    lateinit var shopRepository: ShopRepository
    override fun getShop(): List<Shop> {
        return shopRepository.findAll().filterIsInstance(Shop::class.java)
    }
}