package cmuguide.project.dao

import cmuguide.project.entity.Review

interface ReviewDao {
     fun getAllReviews(): List<Review>
     fun save(review: Review): Review
     fun findById(id: Long): Review
}