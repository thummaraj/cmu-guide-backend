package cmuguide.project.dao

import cmuguide.project.entity.User

interface UserDao {
     fun findByEmail(email: String): User
}