package cmuguide.project.dao

import cmuguide.project.entity.Shop

interface ShopDao {
     fun getShop(): List<Shop>
     fun getShopByShopId(shopId: String): Shop
}