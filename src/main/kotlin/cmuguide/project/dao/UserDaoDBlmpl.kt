package cmuguide.project.dao

import cmuguide.project.entity.User
import cmuguide.project.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class UserDaoDBlmpl: UserDao {
    @Autowired
    lateinit var userRepository: UserRepository
    override fun findByEmail(email: String): User {
        return userRepository.findByEmailContaining(email)
    }
}