package cmuguide.project.service

import cmuguide.project.entity.Shop

interface ShopService {
     fun getShop(): List<Shop>
     fun getShopByShopId(shopId: String): Shop
}