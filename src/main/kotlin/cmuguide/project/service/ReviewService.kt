package cmuguide.project.service

import cmuguide.project.entity.Review
import cmuguide.project.entity.User
import cmuguide.project.entity.dto.ReviewDto

interface ReviewService {
     fun getAllReviews(): List<Review>
     fun save(review: Review): Review
     fun save(shopId:String, review: Review): Review
     fun save(shopId:String,email:String, review: Review): Review
     fun editReview(id:Long,review: ReviewDto): Review
     fun findById(id: Long): Review
     fun remove(id: Long): Review
}