package cmuguide.project.service

import cmuguide.project.dao.ReviewDao
import cmuguide.project.dao.ShopDao
import cmuguide.project.dao.UserDao
import cmuguide.project.entity.Review
import cmuguide.project.entity.User
import cmuguide.project.entity.dto.ReviewDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ReviewServicelmpl: ReviewService {
    @Transactional
    override fun save(shopId: String, email: String, review: Review): Review {
        val shop = shopDao.getShopByShopId(shopId)
        val user = userDao.findByEmail(email)
        val review = reviewDao.save(review)
        review.user = user
        shop?.reviews?.add(review)
        return review
    }

    @Autowired
    lateinit var userDao: UserDao

    @Transactional
    override fun remove(id: Long): Review {
        val review = reviewDao.findById(id)
        review?.isDeleted = true
        return review
    }

    override fun findById(id: Long): Review {
        return reviewDao.findById(id)
    }

    override fun editReview(id:Long,review: ReviewDto): Review {
        val edit = reviewDao.findById(id)
        edit.review = review.review
        edit.rating = review.rating
        return reviewDao.save(edit)
    }

    @Autowired
    lateinit var shopDao: ShopDao
    @Transactional
    override fun save(shopId: String, review: Review): Review {
        val shop = shopDao.getShopByShopId(shopId)
        val review = reviewDao.save(review)
        shop?.reviews?.add(review)
        return review
    }

    override fun save(review: Review): Review {
        return reviewDao.save(review)
    }

    @Autowired
    lateinit var reviewDao: ReviewDao
    override fun getAllReviews(): List<Review> {
        return reviewDao.getAllReviews()
    }
}