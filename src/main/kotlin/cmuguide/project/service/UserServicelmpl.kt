package cmuguide.project.service

import cmuguide.project.dao.UserDao
import cmuguide.project.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServicelmpl: UserService {
    @Autowired
    lateinit var userDao: UserDao
    override fun findByEmail(email: String): User {
        return userDao.findByEmail(email)
    }
}