package cmuguide.project.service

import cmuguide.project.entity.User

interface UserService {
     fun findByEmail(email: String): User
}