package cmuguide.project.service

import cmuguide.project.dao.ShopDao
import cmuguide.project.entity.Shop
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShopServicelmpl: ShopService {
    override fun getShopByShopId(shopId: String): Shop {
        return shopDao.getShopByShopId(shopId)
    }

    @Autowired
    lateinit var shopDao: ShopDao
    override fun getShop(): List<Shop> {
        return shopDao.getShop()
    }
}